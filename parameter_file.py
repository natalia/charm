#PARAMETER FILE
verbose = True

#data file
fdata = 'data.txt'

#covariance matrix containing systematics
fCovSys = 'Cov-syst.txt'

#size of pixels in signal space
dist = 1e-3
