Charm (Cosmic history agnostic reconstruction method)

NIFTY application to reconstruct the cosmic expansion history

charm is licensed under the
`GPLv3 <http://www.gnu.org/licenses/gpl.html>`
==========================================

**NIFTY** project homepage: `<http://www.mpa-garching.mpg.de/ift/nifty/>`_

..........

Summary
............
Charm (cosmic history agnostic reconstruction method) is a Python code
to reconstruct the cosmic expansion history in the framework of
Information Field Theory. The reconstruction is performed via the
iterative Wiener filter from an agnostic or from an informative prior.
The charm code allows one to test the compatibility of several different data sets with the LambdaCDM model in a non-parametric way.

Requirements
............
*   NIFTY can be installed using `PyPI <https://pypi.python.org/pypi>`_ and
    **pip** by running the following command::

        pip install ift_nifty

    Alternatively, a private or user specific installation can be done by::

        pip install --user ift_nifty


Usage
...........

run:

python charm.py 

and it generates the Figure 5 of the paper.

Other data and covariance matrices can be used by changing the names 
of the files in the parameter_file.py


References
..........

`arXiv:1608.04007 <https://arxiv.org/abs/1608.04007>`_
